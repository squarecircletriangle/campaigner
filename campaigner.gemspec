# -*- encoding: utf-8 -*-
$:.push File.expand_path('../lib', __FILE__)
require 'campaigner/version'

Gem::Specification.new do |s|
  s.name        = 'campaigner'
  s.version     = Campaigner::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['Nick Marfleet']
  s.email       = ['nick@blocksglobal.com']
  s.homepage    = ''
  s.summary     = %q{Worker gem for receiving, processing and publishing campaign recipients}
  s.description = %q{Worker gem for receiving, processing and publishing campaign recipients}

  s.rubyforge_project = 'campaigner'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ['lib']
 
  s.add_runtime_dependency 'bottle', '~>0.0.15'
  s.add_runtime_dependency 'pg', '~>0.14.1'
  s.add_runtime_dependency 'sequel', '~>3.43.0'
  s.add_runtime_dependency 'erubis', '~>2.7.0'
#  s.add_runtime_dependency 'submarine', '~>0.0.5'
  s.add_runtime_dependency 'mail', '~>2.5.3'
  s.add_runtime_dependency 'bugsnag', '~>1.2.12'

  s.add_development_dependency 'rspec', '~>2.6.0'
  
end
