FROM ruby:2.1.9-alpine

MAINTAINER Nick Marfleet

#cache buster:wq
ENV SYSTEM_UPDATE=5

RUN apk add --update \
	alpine-sdk \
	postgresql-dev \
	libpq \
	git 

RUN mkdir /etc/campaigner
WORKDIR /etc/campaigner
COPY docker/Gemfile ./Gemfile
COPY docker/Gemfile.lock ./Gemfile.lock

RUN gem update --system && \
	gem update bundler && \
	bundle install --deployment --jobs 4 --retry 5

ENTRYPOINT ["campaigner", "start"]
CMD ["-e", "production"]
