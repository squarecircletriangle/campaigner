## Start running campaigner:
bundle exec bin/campaigner

## Run a client to throw messages at it...
require 'bottle'

campaign_data = {
    :name                => 'name',
    :blocks_id           => '123',
    :client              => 'blocks.sct.com.au',
    :total_recipients    => 1000,
    :template            => 'a load of content should be able to go in here.html',
    :requires_processing => false,
    :subject             => 'subject!',
    :from                => 'Nick Marfleet <nick@sct.com.au>',
    :sender_address      => 'campaign_sender_address@blocksglobal.com'
}

recipient_data = {
    :campaigner_id => 2,
    :contact       => { :email => 'nick@sct.com.au' },
    :unique_id     => '123xyz'
}

c = Bottle::Client.new('sct-home', 'blocks.campaigner')

c.dispatch('campaign', campaign_data) do |data|
  puts "MY DATA LIKES TO SAY: #{data.inspect}"
end

c.dispatch('recipient', recipient_data) do |data|
  puts "MY DATA LIKES TO SAY: #{data.inspect}"
end