Sequel.migration do
  change do
    create_table(:recipients) do
      primary_key :id
      String :recipient_id, :null => false
      Integer :campaigner_id, :null => false
    end
  end
end