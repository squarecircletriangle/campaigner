Sequel.migration do
  change do
    create_table(:campaigns) do
      primary_key :id
      String :name, :null => false
      Integer :blocks_id, :null => false
      String :client, :null => false
      Integer :total_recipients
      String :template, :text => true
      TrueClass :requires_postrendering, :default => false
      String :subject, :null => false
      String :from, :null => false
      String :sender_address, :null => false
    end
  end
end