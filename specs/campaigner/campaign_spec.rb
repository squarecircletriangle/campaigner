require File.dirname(__FILE__) + '/../spec_helper.rb'

describe Campaigner::Campaign do

  before do
    @args = {
        :name                   => 'campaign name',
        :blocks_id              => '123',
        :client                 => 'blocks.sct.com.au',
        :total_recipients       => 1000,
        :template               => '<p>a load of html content should be able to go in here</p>',
        :requires_postrendering => false,
        :subject                => 'subject!',
        :from                   => 'Nick Marfleet <nick@sct.com.au>',
        :sender_address         => 'campaign_sender_address@blocksglobal.com'
    }
  end

  describe 'self.clean_args' do
    it 'should remove hash keys that are not recognised by the model' do
      Campaigner::Campaign.clean_args(@args.merge(:cheese => 'is blue')).should == @args
    end

    it 'should convert keys to symbols' do
      args = @args.inject({}) { |m, (k, v)| m[k.to_s] = v; m }
      Campaigner::Campaign.clean_args(args).should == @args
    end
  end

end