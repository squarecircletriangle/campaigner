require File.dirname(__FILE__) + '/../../spec_helper.rb'

Mail.defaults do
  delivery_method :test
end

describe Campaigner::Workers::Recipient do

  before do
    @recipient_worker = described_class.new
    @campaign         = Campaigner::Campaign.new(
        :name                   => 'campaign name',
        :blocks_id              => '123',
        :client                 => 'blocks.sct.com.au',
        :total_recipients       => 1000,
        :template               => '<p>a load of html content should be able to go in here</p>',
        :requires_postrendering => false,
        :subject                => 'subject!',
        :from                   => 'Nick Marfleet <nick@sct.com.au>',
        :sender_address         => 'campaign_sender_address@blocksglobal.com'
    )
    @args             = {
        :campaigner_id => 999,
        :contact       => { :email => 'nick@sct.com.au' },
        :unique_id     => '123xyz'
    }
  end

  it 'should inherit from Bottle::Foreman' do
    described_class.superclass.should == Bottle::Foreman
  end

  describe 'process' do

    before do
      @recipient_worker.stub!(:send_email)
      @campaign.stub!(:id).and_return(123)
      Campaigner::Campaign.stub!(:[]).and_return(@campaign)
    end

    it 'should return an error message if the campaign could not be found' do
      Campaigner::Campaign.stub!(:[]).and_return(nil)
      @recipient_worker.process(@args).should == { :state => 'error', :message => 'Could not find campaign for campaigner_id: 999' }
    end

    it 'should return an error message if the recipient address is empty' do
      @args[:contact][:email] = ''
      @recipient_worker.process(@args).should == { :state => 'error', :message => 'No valid recipient address provided' }
    end

    it 'should process the template against the recipients contact details plus recipient unique_id if postrendering is required' do
      @campaign.requires_postrendering = true
      @campaign.template.should_receive(:postrender).with(@args[:contact].merge(:recipient_unique_id => @args[:unique_id]))
      @recipient_worker.process(@args)
    end

    it 'should send an email to the recipient' do
      @recipient_worker.should_receive(:send_email)
      @recipient_worker.process(@args)
    end

    it 'should return successfully' do
      @recipient_worker.process(@args).should == { :state => 'success', :unique_id => '123xyz' }
    end
  end

  describe 'send_email' do

    before do
      @expected_attrs = {
          :from        => @campaign.from,
          :to          => @args[:contact][:email],
          :subject     => @campaign.subject,
          :message_id  => "<#{@args[:unique_id]}@#{@campaign.client}>",
          :sender      => @campaign.sender_address,
          :return_path => @campaign.sender_address
      }
    end

    [:subject, :sender, :return_path].each do |attr|
      it "should set '#{attr.to_s}'" do
        @recipient_worker.send_email(@campaign, @campaign.template, @args)
        email = Mail::TestMailer.deliveries.first
        email.send(attr).should == @expected_attrs[attr]
      end
    end

    it 'should set message_id' do
      @recipient_worker.send_email(@campaign, @campaign.template, @args)
      email = Mail::TestMailer.deliveries.first
      email.send('message_id').should == "#{@args[:unique_id]}@#{@campaign.client}"
    end

    it "should set 'to'" do
      @recipient_worker.send_email(@campaign, @campaign.template, @args)
      email = Mail::TestMailer.deliveries.first
      email.send(:to).should == [@expected_attrs[:to]]
    end

    it "should set 'from'" do
      @recipient_worker.send_email(@campaign, @campaign.template, @args)
      email = Mail::TestMailer.deliveries.first
      email.header[:from].to_s.should == @expected_attrs[:from]
    end


    it 'should generate the html part' do
      @recipient_worker.should_receive(:generate_html_body)
      @recipient_worker.send_email(@campaign, @campaign.template, @args)
    end

    it 'should set the text part' do
      email    = @recipient_worker.send_email(@campaign, @campaign.template, @args)
      textpart = email.parts.detect { |p| p.header['Content-Type'].value == 'text/plain' }
      textpart.body.should == described_class::DEFAULT_TEXT_PART % "http://#{@campaign.client}/recipients/#{@args[:unique_id]}/show_campaign"
    end

  end

end
