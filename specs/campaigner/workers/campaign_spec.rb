require File.dirname(__FILE__) + '/../../spec_helper.rb'
require 'ostruct'

describe Campaigner::Workers::Campaign do

  before do
    @campaign_worker = described_class.new
    @args            = {
        :name                   => 'campaign name',
        :blocks_id              => '123',
        :client                 => 'blocks.sct.com.au',
        :total_recipients       => 1000,
        :template               => '<p>a load of html content should be able to go in here</p>',
        :requires_postrendering => false,
        :subject                => 'subject!',
        :from                   => 'Nick Marfleet <nick@sct.com.au>',
        :sender_address         => 'campaign_sender_address@blocksglobal.com'
    }
  end

  it 'should inherit from Bottle::Foreman' do
    described_class.superclass.should == Bottle::Foreman
  end

  describe 'process' do

    it 'should create a new campaign record' do
      Campaigner::Campaign.should_receive(:create).with(@args)
      @campaign_worker.process(@args)
    end

    it "should return a hash containing the new campaign's id and a success flag" do
      Campaigner::Campaign.stub!(:create).and_return(OpenStruct.new(:id => 100))
      @campaign_worker.process(@args).should == { :state => 'success', :campaigner_id => 100 }
    end

    context 'bad payload' do

      it 'should return an error message if the payload is nil, or not a hash' do
        @campaign_worker.process(nil).should == { :state => 'error', :message => 'Campaign payload expected a hash, got NilClass.' }
        @campaign_worker.process([]).should == { :state => 'error', :message => 'Campaign payload expected a hash, got Array.' }
      end

      it "should return an error message if the payload doesn't contain all required fields'" do
        @args.delete(:blocks_id)
        @campaign_worker.process(@args).should == { :state => 'error', :message => 'Invalid Campaign attributes: blocks_id is not present' }
      end

      it "should ignore invalid fields'" do
        weird_args = @args.merge(:junk => 'foobar', 'more' => 'cheese')
        @campaign_worker.process(weird_args).should == { :state => 'success', :campaigner_id => Campaigner::Campaign.order(:id).last.id }
      end

      it 'should handle unexpected fatal errors' do
        Campaigner::Campaign.stub!(:create).and_raise 'Some bad thing happened'
        @campaign_worker.process(@args).should == { :state => 'error', :message => 'Fatal error handling campaign: Some bad thing happened' }
      end

    end
  end
end