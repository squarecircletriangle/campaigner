require 'bundler'
Bundler.setup(:default, :test)

$:.unshift(File.dirname(__FILE__) + '/../lib')
require 'campaigner'

module Bugsnag
  def self.notify(exception, opts = {})
    # do nothing.
  end
end

Campaigner.start(:test)
