require 'bottle'
require 'erubis'
require 'sequel'
require 'pg'
require 'mail'
require 'submarine'

module Campaigner
  @@env = 'development'

  @@connection = nil
  @@opts       = {}

  def self.connection
    @@connection
  end

  def self.configure_database
    connection.disconnect if !!connection
    db_config = {
        adapter:  'postgres',
        database: 'campaigner_' + @@env.to_s,
        encoding: 'utf8'
    }
    %w[host port database user password].each do |key|
      opts_key_name         = ('db_' + key).to_sym
      db_config[key.to_sym] ||= @@opts[opts_key_name]
    end

    puts "connecting to #{db_config.inspect}"
    @@connection = ::Sequel.connect(db_config)
  end

  def self.configure_mail
    if @@opts[:smtp_server]
      smtp_settings = {
          :address              => @@opts[:smtp_server],
          :port                 => 25,
          :domain               => @@opts[:smtp_server],
          :enable_starttls_auto => false
      }

      ::Mail.defaults do
        delivery_method :smtp, smtp_settings
      end
    end
  end

  def self.run(opts={})
    puts opts.inspect
    start(opts[:env], opts)
    server = Bottle::Server.new(opts[:queue_name], { :host => opts[:broker] } )
    server.poll
  end

  def self.start(env, opts = {})
    @@env  = env.to_sym
    @@opts = opts
    configure_database
    configure_mail
    require File.dirname(__FILE__) + '/campaigner/campaign'
    require File.dirname(__FILE__) + '/campaigner/recipient'
  end

  def stop
    puts 'Shutting down.......'
    exit
  end
end


require File.dirname(__FILE__) + '/campaigner/spawn_server'
require File.dirname(__FILE__) + '/campaigner/workers/campaign'
require File.dirname(__FILE__) + '/campaigner/workers/recipient'

require 'bugsnag'
Bugsnag.configure do |config|
  config.api_key = '3cd69ec299c0fa7c2d44ebed61044aa9'
end
