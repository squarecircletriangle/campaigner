module Campaigner
  module Workers
    class Campaign < Bottle::Foreman

      def process(payload)
        return failure("Campaign payload expected a hash, got #{payload.class.to_s}.") unless payload.is_a?(Hash)

        campaign_data = Campaigner::Campaign.clean_args(payload)
        camp          = Campaigner::Campaign.create(campaign_data)

        success(:campaigner_id => camp.id)

      rescue Sequel::DatabaseDisconnectError => sdde
        i ||= 0
        if i < 3
          i += 1
          Campaigner.configure_database
          retry
        else
          Bugsnag.notify(svf)
          failure("Unable to connect to campaigner database: #{sdde.message}")
        end
      rescue Sequel::ValidationFailed => svf
        Bugsnag.notify(svf)
        failure("Invalid Campaign attributes: #{svf.message}")
      rescue => e
        Bugsnag.notify(e)
        failure("Fatal error handling campaign: #{e.message}")
      end

    end
  end
end
