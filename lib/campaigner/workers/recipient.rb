module Campaigner
  module Workers
    class Recipient < Bottle::Foreman
      DEFAULT_TEXT_PART = 'It seems that you cannot view html email.  You can view this email online here: %s'

      def process(payload)
        campaign = Campaigner::Campaign[payload[:campaigner_id]]
        return failure("Could not find campaign for campaigner_id: #{payload[:campaigner_id]}") if campaign.nil?
        return failure('No valid recipient address provided') if payload[:contact][:email].blank?
        
        recipient = Campaigner::Recipient[:recipient_id => payload[:unique_id]]
        return failure('Recipient delivery record already exists') unless recipient.nil?

        html_content = campaign.requires_postrendering ? campaign.template.postrender(payload[:contact].merge(:recipient_unique_id => payload[:unique_id])) : campaign.template

        send_email(campaign, html_content, payload)

        Campaigner::Recipient.create({
          :campaigner_id => payload[:campaigner_id],
          :recipient_id => payload[:unique_id]
        })

        success({ :unique_id => payload[:unique_id] })
      rescue => e
        Bugsnag.notify(e)
        failure("Fatal error handling campaign: #{e.message}")
      end

      def send_email(campaign, html_content, payload)
        online_address = "http://#{campaign.client}/recipients/#{payload[:unique_id]}/show_campaign"
        viewed_url     = "http://#{campaign.client}/email_response/#{payload[:unique_id]}/read"

        html_body = generate_html_body(html_content, online_address, viewed_url, payload[:forward_message])

        ::Mail.deliver do
          from        campaign.from
          to          payload[:contact][:email]
          subject     campaign.subject
          message_id  "<#{payload[:unique_id]}@#{campaign.client}>"
          sender      campaign.sender_address
          return_path campaign.sender_address

          html_part do
            content_type 'text/html; charset=UTF-8'
            body html_body
          end

          text_part do
            body(DEFAULT_TEXT_PART % online_address)
          end
        end
      end

      private ###################################

      def generate_html_body(html_content, online_address, viewed_url, forward_message = nil)

        html_body = <<-EOF
        <div class="email_forward_message" style="padding: 10px 10px 25px 10px;">
        <p><a href="<%= online_address %>">Can't see this email properly? Click here</a></p><img src="<%= viewed_url %>" width="0" height="0" />

        <% if forward_message %>
          <p><%= forward_message %></p>
          <% end %>

          </div>

          <%= html_content %>
        EOF

        ::Erubis::Eruby.new(html_body).result(binding())
      end
    end
  end
end
