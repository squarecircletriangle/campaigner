module Campaigner
  class Campaign < Sequel::Model
    plugin :validation_helpers

    def validate
      super
      validates_presence [:name, :blocks_id, :client, :total_recipients, :template, :requires_postrendering, :subject, :from, :sender_address]
    end

    def self.clean_args(attrs)
      camp_cols     = Campaigner::Campaign.columns
      attrs.inject({}) do |m, (k, v)|
        k    = k.to_sym unless k.is_a?(Symbol)
        m[k] = v if camp_cols.include?(k)
        m
      end
    end
  end

end