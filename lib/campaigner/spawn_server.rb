module Campaigner
  class SpawnServer

    def start(opts)
      opts = opts.kind_of?(Array) ? opts.shift : opts
      Campaigner.run(opts)
    end

    def stop
      # stop your bad self
    end
  end
end
